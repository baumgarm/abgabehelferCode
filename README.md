<!--
SPDX-FileCopyrightText: 2021 Michael Baumgartner <baumgarm@fim.uni-passau.de>

SPDX-License-Identifier: Apache-2.0
-->

# Abgabehelfer für Code in Programmierung
Dieses Tool soll die Abgabe der eigenen Lösung von Übungsblättern in Programmierung erleichtern. Die Bearbeitung der Aufgaben muss zuvor gemacht werden und wird durch das Tool nicht erleichtert.
## Voraussetzung zur Nutzung
### Anforderungen an die gestellten Aufgaben

1. Die Aufgaben sind in Java-Projekten zu bearbeiten.
2. Die Projekte enthalten die gradle-Tasks "codeToPDF" und "solutionZip", um die Abgabedateien für das jeweilige Projekt zu erzeugen. (Die Dateien daraus sind danach unter build/pdf bzw. build/distributions" zu finden.)

Diese Anforderungen sind beispielsweise bei folgenden Modulen / Lehrenden erfüllt:

| Module | Lehrende |
| ------ | -------- |
| Programmierung I | Dr. Armin Größlinger |
### Eigene Verwaltung der Projektordner
1. Die Ordner mit den einzelnen Projekten sind in einem gemeinsamen Ordner.
2. In diesem gemeinsamen Ordner befinden sich am besten nur die Projektordner und sonst keine weiteren Ordner.
3. Benennung: "B\[Blattnummer\]_A*" Zum Beispiel "B1_A1_IrgendeinName" für eine Aufgabe zum erste Übungsblatt. 
4. Die Datei collect.py sollte auch in diesen gemeinsamen Ordner abgelegt werden.
### Installationen
Es muss Python 3 auf dem Rechner installiert sein. Die neuste Version von Python 3 kann [hier](https://www.python.org/downloads/) heruntergeladen werden. Bei den meisten Linux-Distributionen ist python3 als Paket verfügbar und teils schon vorinstalliert. Die Software ist für alle gängigen Desktopbetriebssysteme (wie z.B. Linux, macOS und Windows) verfügbar.

Auf dem [Rechnerpools der FIM](https://www.fim.uni-passau.de/it-dienste/rechnerpools/) ist auf den Linux-PCs Python 3 installiert. Somit kann das Tool auch dort genutzt werden.
## Verwendung des Tools
### Aufruf
Die Datei collect.py muss nach dem Download im Ordner mit den Projektordnern aufgerufen werden. Möglichkeiten dazu:
 - Eine Shell / Cmd in dem Ordner öffnen und "python3 collect.py" + Enter eingeben
 - Eine Shell / Cmd in dem Ordner öffnen und "collect.py" + Enter eingeben
 - Doppelklick auf die Datei collect.py und "Ausführen" als Option wählen

Sollte es bei der zweiten oder dritten Methode zu Problemen kommen, kann unter Unix-Systemen (z.B. Linux oder macOS) ein fehlendes Ausführrecht der Grund sein. Abhilfe kann dann z.B. der Aufruf "chmod u+x collect.py" bringen. Ansonsten kann auch auf die erste Möglichkeit zurückgegriffen werden, welche problemloser funktioniert.
### Bedienung
Die Anwendung fragt nach der Nummer des Übungsblatts. Diese eingeben und Enter drücken.

Daraufhin erfolgt die Ausführung der gradle-Tasks. Zum Abschluss ist der vollständige Pfad zum Abgabe-Ordner zu sehen. Dieser sollte sich in dem Ordner befinden, wo die collect.py liegt.

Im Abgabe-Ordner sollten bei fehlerfreier Ausführung die ZIP und PDF Dateien zu jedem Projekt des Übungsblatts liegen. Beim Upload die Datei für etwailige Textaufgaben nicht vergessen!
## Fehler und Verbesserungsvorschläge
Es wird die [Ticketverwaltung](https://git.fim.uni-passau.de/baumgarm/abgabehelferCode/-/issues) des Projekts im GitLab genutzt.
### Fehler / Probleme
Sollte es bei der Nutzung des Tools zu Fehler kommen, ist zuerst ein Blick in die [Fehlertickets](https://git.fim.uni-passau.de/baumgarm/abgabehelferCode/-/issues?scope=all&state=all&label_name[]=Bug) zu werfen. Wenn es das Problem schon mal gab, ist dort die Lösung zu finden. Gibt es ein Ticket, welches jedoch noch keine Lösung enthält, kann man die Benachrichtigungen im Ticket aktivieren, um bei Updates informiert zu werden.

Nur wenn es noch kein Ticket gibt, sollte eines erstellt werden. Dabei ist das Label "Bug" zu verwenden. Das Problem sollte möglichst genau beschrieben werden. Die Fehler können dabei ihre Ursache im Tool oder in der Umgebung bzw. Benutzung haben. Je nachdem wird dann ein Bugfix im Tool vorgenommen oder eine Anleitung zur Lösung des Problems auf dem eigenen Rechner gegeben.
### Verbesserungsvorschläge
Verbesserungsvorschläge sollte auch als Tickets erfolgen. Dabei auch zuerst prüfen, ob es dazu nicht vielleicht bereits ein Ticket gibt. Wenn nicht, dann sollte ein Ticket mit passendem [Label](https://git.fim.uni-passau.de/baumgarm/abgabehelferCode/-/labels) erstellt werden. Meistens passt wohl das Label "Funktion".
### Mit wirken
Es kann gerne selbst zum Projekt beigetragen werden. Es gelten dabei folgende Grundsätze:
- Jede Änderung braucht ein Ticket. Dieses ist in allen Commits zu der jeweiligen Änderung (neue Funktion, Bugfix, ...) anzugeben.
- Dann sollte das Ticket vielleicht erst etwas diskutiert werden. 
	- Ist die Anforderung zu speziell? :arrow_right: Kann sie allgemeiner (konfigurierbar) gestaltet werden?
	- Was muss ggf. getestet werden?
	- ...
- Commit Messages sollten beschreiben, was / wieso etwas (aus technischer Sicht) gemacht wurde. (Guter Vortrag zu dem Thema: [How To Commit: Nachvollziehbare Git-Historien](https://chemnitzer.linux-tage.de/2019/de/programm/beitrag/196))
- Projekt forken :arrow_right: Änderungen im eigen Fork vornehmen :arrow_right: Pull Request
