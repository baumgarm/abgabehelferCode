#!/usr/bin/env python3

# SPDX-FileCopyrightText: 2021 Michael Baumgartner <baumgarm@fim.uni-passau.de>
#
# SPDX-License-Identifier: Apache-2.0

import os, platform, fnmatch

# Welches Blatt soll gemacht werden?
## Gefuehrter Modus (aktuell der einzigste)
print("Fuer welches Blatt soll die Codeabgabe zusammengestellt werden?")
BlattNum = input("Bitte Nummer angeben: ")
## Ordner mit den Aufgaben zum Blatt zusammen suchen
AufgabenOrdner = list(os.listdir("."))
if len(AufgabenOrdner) < 1:
    print("Keine Dateien im Verzeichnis " + os.path.abspath(".") + " gefunden.")
    print("==== ABBRUCH ====")
else:
    OrdnerPattern = "B" + BlattNum + "_A*"
    def filter_Dateien(Datei):
        return fnmatch.fnmatchcase(Datei, OrdnerPattern)
    AufgabenOrdner = list(filter(filter_Dateien, AufgabenOrdner))

    if len(AufgabenOrdner) < 1:
        print("Keine passenden Ordner (" + OrdnerPattern + ") im Verzeichnis " + os.path.abspath(".") + " gefunden.")
        print("==== ABBRUCH ====")
    else:
        # Ordner gefunden, also kann die Ausfuehrung starten
        ## Ausgabeordner bei Bedarf noch erstellen
        if not os.path.exists("Abgabe"):
            os.makedirs("Abgabe")
        ## Absolute Pfade fuer eindeutigen Zugriff
        Output = os.path.abspath("Abgabe")

        Fails = [];
        # Liste durchlaufen
        for Ordner in AufgabenOrdner:
            print("=== Start Projekt: " + Ordner + " ===")            
            ## Tasks ausfuehren
            os.chdir(Ordner)
            if platform.system() == "Windows":  
                gradlew = "\gradlew.bat"
            else:
                gradlew = "./gradlew"
            ## Projekte ohne Gradle nicht zum Abbruch des ganzen Skripts führen lassen
            if os.path.exists(gradlew): 
                if platform.system() != "Windows":  
                    os.system("chmod u+x " + gradlew)
                os.system(gradlew + " codeToPDF")
                os.system(gradlew + " solutionZip")
                ## Abgabedateien raus ziehen und leere Ordner loeschen
                os.chdir("build")
                for Abgabeordner in ["pdf", "distributions"]:
                    os.chdir(Abgabeordner)
                    for Abgabe in os.listdir("."):    
                        os.rename(Abgabe, os.path.join(Output, Abgabe))
                    os.chdir("..")
                    os.rmdir(Abgabeordner)
                os.chdir("..") # aus dem build Ordner raus
            else:
                Fails.append(Ordner)
                print("Projekt hat kein gradlew")
            print("=== Ende Projekt: " + Ordner + " ===")    
            os.chdir("..") # aus dem Projekt Ordner raus
        print("Abgabedateien zusammengestellt in " + Output)
        if len(Fails) > 0:
            print("FEHLENDE Projekte darin:")
            ## fehlgeschlage Projekte als Stichpunktliste ausgeben
            print(" - " + "\r\n - ".join(Fails))
        print("!! Beim Upload das PDF mit den Textaufgaben nicht vergessen, falls Textaufgaben vorhanden sind !!")
input("Press Enter for Exit")
